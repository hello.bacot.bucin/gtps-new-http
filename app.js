const express = require('express');
const path = require('path');
const fs = require('fs')
const https = require('https')
const http = require('http')
const app = express();

var port = 443;

var options = {
    key: fs.readFileSync(__dirname + '/ssl/private.key', 'utf8'),
    cert: fs.readFileSync(__dirname + '/ssl/public.cert', 'utf8'),
};

var server = https.createServer(options, app).listen(port, function(){
    console.log("Express server listening on port " + port);
  });

app.get('/assets', function(req, res) {
    res.sendFile(path.join(__dirname, '/assets/'));
  });

app.get('/', function(req, res) {
    res.set("Connection", "close");
    res.end();
});


console.log('Server started at http://localhost:' + port);